﻿namespace Project.ApplicationRequirements.DataTransferObjects;

using Kelly.Data.Schnittstellen;

public record ToolClassAndPathViewDataTransferObject : IDataTransferObject
{

    public string BatchFileCommand{get; set;}

    public string ClassName{get; set;}

    public string Directory{get; set;}

    public int Id{get; set;}

    public bool IsNamedCorrectly{get; set;}

}