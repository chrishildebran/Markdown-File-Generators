﻿namespace Project.ApplicationRequirements.DataTransferObjects;

using Kelly.Data.Schnittstellen;

public record FeatureNameDataTransferObject : IDataTransferObject
{

    public string Adjective{get; set;}

    public string Adjective2{get; set;}

    public int? ApplicationId{get; set;}

    public string ButtonClassName{get; set;}

    public DateTime? ButtonClassNameCheck{get; set;}

    public string ButtonTitleName{get; set;}

    public string Category{get; set;}

    public string ClassName{get; set;}

    public DateTime? ClassNameCheck{get; set;}

    public string ClassType{get; set;}

    public string Conjunction{get; set;}

    public string DiagrammingLink{get; set;}

    public DateTime? DiagrammingLinkCheck{get; set;}

    public int Id{get; set;}

    public string LastTested{get; set;}

    public string Noun{get; set;}

    public string Noun2{get; set;}

    public string ToolName{get; set;}

    public string ToolNameComputed{get; set;}

    public string ToolNamePrevious{get; set;}

    public string TrackingLink{get; set;}

    public DateTime? TrackingLinkCheck{get; set;}

    public string Verb{get; set;}

}