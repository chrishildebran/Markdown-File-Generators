﻿namespace Project.ApplicationRequirements.DataTransferObjects;

using Kelly.Data.Schnittstellen;

public record ApplicationToolDataTransferObject : IDataTransferObject
{
    public int ApplicationId { get; set; }

    public string ClassNameActual { get; set; }

    public int Id { get; set; }

    public string LinearLabelActual { get; set; }

    public string Name { get; set; }

    public string Notes { get; set; }

    public string Noun { get; set; }
    public string RootDirectory { get; set; }

    public string ProjectDirectoryActual { get; set; }

    public bool ScopeDocument { get; set; }

    public bool ScopeSelection { get; set; }

    public bool ScopeView { get; set; }
    public bool ScopeApplication { get; set; }

    public string NounSecondary { get; set; }

    public string NounTertiary { get; set; }

    public string TriggeredBy { get; set; }

    public string Triggers { get; set; }

    public string UpNoteNameActual { get; set; }

    public string Verb { get; set; }
}