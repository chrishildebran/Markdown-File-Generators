﻿using Kelly.Data.Schnittstellen;

namespace Project.ApplicationRequirements.DataTransferObjects;

 

public record ToolRequirementTestDataTransferObject : IDataTransferObject
{

    
    public string ApplicationVersion{get; set;}

    public DateTime DateTested{get; set;}

    public int Id{get; set;}

    public int ToolRequirementId{get; set;}

}