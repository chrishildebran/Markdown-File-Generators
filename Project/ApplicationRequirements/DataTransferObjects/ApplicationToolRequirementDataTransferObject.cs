﻿namespace Project.ApplicationRequirements.DataTransferObjects;

using Kelly.Data.Schnittstellen;

public record ApplicationToolRequirementDataTransferObject : IDataTransferObject
{

    public int ApplicationToolId{get; set;}

    public DateTime? DateImplemented{get; set;}

    public DateTime? DateTested{get; set;}

    public int? FeatureNamerId{get; set;}

    public int Id{get; set;}

    public string Notes{get; set;}

    public string Requirement{get; set;}

}