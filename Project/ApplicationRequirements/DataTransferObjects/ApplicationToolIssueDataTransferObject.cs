﻿namespace Project.ApplicationRequirements.DataTransferObjects;

using Kelly.Data.Schnittstellen;

public record ApplicationToolIssueDataTransferObject : IDataTransferObject
{

    public int ApplicationToolId{get; set;}

    public string Cycle{get; set;}

    public DateTime DateImplemented{get; set;}

    public DateTime DateInitiated{get; set;}

    public int? FeatureNamerId{get; set;}

    public int Id{get; set;}

    public string InitiatedBy{get; set;}

    public string Notes{get; set;}

    public string Role{get; set;}

    public string SoThat{get; set;}

    public string WantNeed{get; set;}

}