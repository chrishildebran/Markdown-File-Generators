﻿namespace Project.ApplicationRequirements.DataTransferObjects;

using Kelly.Data.Schnittstellen;

public record ApplicationDataTransferObject : IDataTransferObject
{

    public string Description{get; set;}

    public int Id{get; set;}

    public string Language{get; set;}

    public string LinearUrl{get; set;}

    public string Name{get; set;}

    public string NameFormal{get; set;}

    public string Notes{get; set;}

    public string Owner{get; set;}

    public string RepositoryUrl{get; set;}

    public string SolutionNameCalc{get; set;}

}