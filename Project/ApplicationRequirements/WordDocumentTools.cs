﻿namespace Project.ApplicationRequirements;

using System.Reflection.Metadata;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HtmlToOpenXml;
using Markdig;
using Document = DocumentFormat.OpenXml.Wordprocessing.Document;

internal class WordDocumentTools
{

    public static void CreateHtmlFromMarkdown(string filePath)
    {
        // Step 1: Read the Markdown content
        var markdownContent = File.ReadAllText(filePath);


        // Step 2: Convert Markdown to HTML using Markdig
        var htmlContent = Markdown.ToHtml(markdownContent);

        var wordDocumentPath = filePath.Replace(".md", ".docx");


        // Step 3: Create a Word document and insert the HTML
        CreateWordDocumentFromHtmlSandbox1(wordDocumentPath, htmlContent);

        Console.WriteLine("Conversion completed. The Word document has been created.");
    }

    private static void CreateWordDocumentFromHtmlSandbox1(string wordFilePath, string htmlContent)
    {
        using var wordDoc = WordprocessingDocument.Create(wordFilePath, WordprocessingDocumentType.Document);


        // Add a main document part
        var mainPart = wordDoc.AddMainDocumentPart();

        mainPart.Document = new Document();

        var body = new Body();


        // Convert HTML to OpenXml elements
        var altChunkId = "AltChunkId1";

        var chunk = mainPart.AddAlternativeFormatImportPart(AlternativeFormatImportPartType.Html, altChunkId);

        using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(htmlContent)))
        {
            chunk.FeedData(stream);
        }

        var altChunk = new AltChunk
        {
            Id = altChunkId
        };

        body.Append(altChunk);

        mainPart.Document.Append(body);
        mainPart.Document.Save();
    }

    private static void CreateWordDocumentFromHtmlSandbox2(string filePath, string markdownContent)
    {
        var htmlContent = Markdown.ToHtml(markdownContent);


        // Step 3: Create a Word document and insert the HTML
        using (var wordDoc = WordprocessingDocument.Create(filePath, WordprocessingDocumentType.Document))
        {
            // Add a main document part
            var mainPart = wordDoc.AddMainDocumentPart();
            mainPart.Document = new Document(new Body());


            // Create a body
            var body = mainPart.Document.Body;


            // Use HtmlToOpenXml to parse and add the HTML content
            var converter = new HtmlConverter(mainPart);
            converter.ParseHtml(htmlContent);


            // Save the document
            mainPart.Document.Save();
        }

        Console.WriteLine($"Markdown file has been successfully converted to {filePath}");
    }

}