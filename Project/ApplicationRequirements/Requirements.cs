﻿namespace Project.ApplicationRequirements;

using System.Data;
using System.Text;
using Kelly.Core.WerkZeuge;
using Kelly.Data.TransferServices;
using Markdown2Pdf;
using Markdown2Pdf.Options;
using Project.ApplicationRequirements.DataTransferObjects;
using Project.ApplicationRequirements.Models;
using PuppeteerSharp.Media;
using MarginOptions = Markdown2Pdf.Options.MarginOptions;

internal class Requirements : IDisposable
{

    public Requirements()
    {
        var dbConnection = this.dataTransferService.Connection;

        dbConnection.Open();

        var isOpen = dbConnection.State == ConnectionState.Open;

        Console.WriteLine($"Is Connection Open? => {isOpen}");

        if (isOpen)
        {
            dbConnection.Close();
        }
        else
        {
            throw new Exception("Connection to the database failed");
        }
    }

    ~Requirements()
    {
        this.Dispose();
    }

    private List<ApplicationModel> ApplicationModels{get;} = new();

    public void CreateFile()
    {
        foreach (var application in this.ApplicationModels)
        {
            var docsPath = "C:\\Repo\\kelly\\Kelly-Tools-For-Revit-Project\\doc\\MD";
            var pdfPath  = "C:\\Repo\\kelly\\Kelly-Tools-For-Revit-Project\\doc\\PDF";

            var orderedTools = application.ApplicationToolModels.OrderBy(t => t.Notes);

            ////var stopwatch = new Stopwatch();

            foreach (var tool in orderedTools)
            {
                //// stopwatch.Restart();

                var targetDirectory = new DirectoryInfo($"{docsPath}\\{tool.RootDirectory}");
                var pdfDirectory    = new DirectoryInfo($"{pdfPath}\\{tool.RootDirectory}");

                if (targetDirectory.Exists == false)
                {
                    targetDirectory.Create();
                }

                var sb = new StringBuilder();


                // ------------------------------------------------------------
                // Tool Header
                var toolName = $"{tool.Id:D3} - {tool.Name.ChangeToTitleCaseString(false, false)}";

                sb.Append("# ").AppendLine(toolName);
                sb.AppendLine();

                ////var qwer = tool.ApplicationToolRequirements.Count(t => t.DateImplemented == new DateTime(1900, 1, 1));

                sb.AppendLine("## Description: ");
                sb.AppendLine($"The {tool.Name.ChangeToTitleCaseString(false, false)} tools primary purpose is to...");

                sb.AppendLine();

                sb.AppendLine("## Locations: ");
                sb.Append("* Class Root: ").AppendLine(tool.RootDirectory);
                sb.Append("* Linear Project: ").AppendLine(tool.LinearLabelActual);

                sb.AppendLine();


                // ------------------------------------------------------------
                // Tool Information
                sb.Append("## ").AppendLine("Tool Information");
                sb.Append("* Class Directory: ").AppendLine(tool.ToolClassAndPathView.Directory);
                sb.Append("* Class Name: ").AppendLine(tool.ToolClassAndPathView.ClassName);

                sb.Append("* Notes: ").AppendLine(tool.Notes);
                sb.Append("* Scopes: ").AppendLine(tool.GetScopes());
                sb.Append("\t* Scope Hash: ").AppendLine(tool.GetScopeHash());
                sb.Append("* Triggered By: ").AppendLine(tool.TriggeredBy);
                sb.Append("* Triggers: ").AppendLine(tool.Triggers);
                //// sb.Append("* Class Directory: ".PadRight(20)).AppendLine(tool.ToolClassAndPathView.Directory);
                //// sb.Append("* Project Directory: ").AppendLine(tool.ProjectDirectoryActual);
                //// sb.Append("* Tags: ").AppendLine("#" + tool.Notes.Replace('\\', '/').Replace(" ", ""));
                //// sb.Append("## Unimplemented Requirements: ").AppendLine(qwer.ToString());

                // ------------------------------------------------------------
                // Tool Requirements
                sb.Append("## ").AppendLine("Tool Requirements");

                foreach (var requirement in tool.ApplicationToolRequirements)
                {
                    sb.Append("- [ ] ").AppendLine($"Ability to {requirement.Requirement}");

                    ////foreach (var test in requirement.ToolRequirementsTests)
                    ////{
                    ////    var wasVerified = !Equals(test.DateTested, new DateTime(1900, 1, 1));
                    ////
                    ////    var testedState = wasVerified ? "- [x] " : "- [ ] "; // Keep Trailing Space 
                    ////
                    ////    var verifiedText = wasVerified ? $" - Tested: {test.DateTested.Date.ToShortDateString()}" : " - Testing Required";
                    ////
                    ////    sb.Append(testedState).Append(test.ApplicationVersion).AppendLine(verifiedText);
                    ////}
                }

                sb.AppendLine();


                // ------------------------------------------------------------
                // Issues
                sb.Append("## ").AppendLine("Issues - Send To Linear");
                sb.AppendLine("* ...");


                // ------------------------------------------------------------
                // Footer
                sb.AppendLine("------");
                sb.Append("`Document Created: ").Append(DateTime.Now.ToLongDateString()+ " ").AppendLine(DateTime.Now.ToLongTimeString() + "`");


                // Create Documents
                var mdFilePath  = $"{targetDirectory}\\{toolName}.md";
                var pdfFilePath = $"{pdfDirectory}\\{toolName}.pdf";

                var exists = CreateMarkdown(mdFilePath, sb);

                if (exists == false)
                {
                    throw new Exception("File Not Found");
                }

                //// var path = this.CreatePdf(mdFilePath, pdfFilePath).Result;

                //// WordDocumentTools.CreateHtmlFromMarkdown(mdFilePath);

                //// stopwatch.Stop();

                //// var et = stopwatch.Elapsed.TotalSeconds;

                //// Console.WriteLine($"\t\t\tSeconds = {et}");
            }
        }
    }

    public void Dispose()
    {
        if (this.dataTransferService is IDisposable disposable)
        {
            disposable.Dispose();
        }
        else
        {
            this.dataTransferService.Dispose();
        }
    }

    public void GetData()
    {
        // Get Application Data
        Console.WriteLine("Retrieving Application Data");
        var applicationDtos = this.dataTransferService.SelectAll<ApplicationDataTransferObject>("[namers].[USP_Application_SelectAll_v20250116]");


        // Iterate Application Data
        foreach (var application in applicationDtos)
        {
            this.ApplicationModels.Add(new ApplicationModel(application, this.dataTransferService));
        }
    }

    private static bool CreateMarkdown(string mdFilePath, StringBuilder sb)
    {
        Console.WriteLine($"\tCreating MD: {mdFilePath}");

        File.WriteAllText(mdFilePath, sb.ToString());

        var mdExists = File.Exists(mdFilePath);

        return mdExists;
    }

    private async Task<string> CreatePdf(string mdFileInfo, string pdfFilePath)
    {
        var converter = new Markdown2PdfConverter
        {
            Options =
            {
                ChromePath = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
                MarginOptions = new MarginOptions
                {
                    Top    = "50px",
                    Left   = "50px",
                    Bottom = "50px",
                    Right  = "50px"
                },
                Format          = PaperFormat.Letter,
                Scale           = 0.8m,
                TableOfContents = new TableOfContentsOptions()
            }
        };

        Console.WriteLine($"\tCreating PDF: {pdfFilePath}");

        return await converter.Convert(mdFileInfo, pdfFilePath);
    }

    private readonly SqlServerService dataTransferService = new("Data Source=tcp:s11.everleap.com;Initial Catalog=DB_7753_dev;User ID=DB_7753_dev_user;Password=D90438H738H;Integrated Security=False;TrustServerCertificate=true;");

}