namespace Project.ApplicationRequirements.Models;

using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;
using Project.ApplicationRequirements.DataTransferObjects;

public class ToolClassAndPathViewModel : IDomainModel
{

    public ToolClassAndPathViewModel(ToolClassAndPathViewDataTransferObject dto)
    {
        this.BatchFileCommand = dto.BatchFileCommand;
        this.ClassName        = dto.ClassName;
        this.Directory        = dto.Directory;
        this.Id               = dto.Id;
        this.IsNamedCorrectly = dto.IsNamedCorrectly;
    }

    public string BatchFileCommand{get; set;}

    public string ClassName{get; set;}

    public string Directory{get; set;}

    public int Id{get; set;}

    public bool IsNamedCorrectly{get; set;}

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        throw new NotImplementedException();
    }

}