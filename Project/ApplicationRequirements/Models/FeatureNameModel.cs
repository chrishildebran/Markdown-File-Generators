﻿namespace Project.ApplicationRequirements.Models;

using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;

public class FeatureNameModel : IDomainModel
{

    public string Adjective{get; set;}

    public string Adjective2{get; set;}

    public int? ApplicationId{get; set;}

    public string ButtonClassName{get; set;}

    public DateTime? ButtonClassNameCheck{get; set;}

    public string ButtonTitleName{get; set;}

    public string Category{get; set;}

    public string ClassName{get; set;}

    public DateTime? ClassNameCheck{get; set;}

    public string ClassType{get; set;}

    public string Conjunction{get; set;}

    public string DiagrammingLink{get; set;}

    public DateTime? DiagrammingLinkCheck{get; set;}

    public int Id{get; set;}

    public string LastTested{get; set;}

    public string Noun{get; set;}

    public string Noun2{get; set;}

    public string ToolName{get; set;}

    public string ToolNameComputed{get; set;}

    public string ToolNamePrevious{get; set;}

    public string TrackingLink{get; set;}

    public DateTime? TrackingLinkCheck{get; set;}

    public string Verb{get; set;}

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        throw new NotImplementedException();
    }

}