﻿namespace Project.ApplicationRequirements.Models;

using Project.ApplicationRequirements.DataTransferObjects;
using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;
using Kelly.Data.TransferServices;

public class ApplicationToolRequirementModel : IDomainModel
{

    public ApplicationToolRequirementModel(ApplicationToolRequirementDataTransferObject dataTransferObject, SqlServerService dataTransferService)
    {
        this.ApplicationToolId = dataTransferObject.Id;
        this.DateImplemented   = dataTransferObject.DateImplemented;
        this.DateTested        = dataTransferObject.DateTested;
        this.FeatureNamerId    = dataTransferObject.FeatureNamerId;
        this.Id                = dataTransferObject.Id;
        this.Notes             = dataTransferObject.Notes;
        this.Requirement       = dataTransferObject.Requirement;

        var storedProcedure = "[namers].[USP_ToolRequirementTest_SelectWhereApplicationToolRequirementIdEquals_v20250120]";

        var parameters = new
        {
            ToolRequirementId = this.Id
        };

        var tests = dataTransferService.SelectMany<ToolRequirementTestDataTransferObject>(storedProcedure, parameters);

        foreach (var test in tests)
        {
            this.ToolRequirementsTests.Add(new ToolRequirementTestModel(test));
        }

        var stop = true;
    }

    public int ApplicationToolId{get; set;}

    public DateTime? DateImplemented{get; set;}

    public DateTime? DateTested{get; set;}

    public int? FeatureNamerId{get; set;}

    public int Id{get; set;}

    public string Notes{get; set;}

    public string Requirement{get; set;}

    public List<ToolRequirementTestModel> ToolRequirementsTests{get; set;} = new();

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        throw new NotImplementedException();
    }

}