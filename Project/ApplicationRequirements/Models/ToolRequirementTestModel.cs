﻿namespace Project.ApplicationRequirements.Models;

using Project.ApplicationRequirements.DataTransferObjects;
using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;

public class ToolRequirementTestModel : IDomainModel
{

    public ToolRequirementTestModel(ToolRequirementTestDataTransferObject dataTransferObject)
    {
        this.Id                 = dataTransferObject.Id;
        this.ToolRequirementId  = dataTransferObject.ToolRequirementId;
        this.ApplicationVersion = dataTransferObject.ApplicationVersion;
        this.DateTested         = dataTransferObject.DateTested;
    }

    public string ApplicationVersion{get; set;}

    public DateTime DateTested{get; set;}

    public int Id{get; set;}

    public int ToolRequirementId{get; set;}

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        throw new NotImplementedException();
    }

}