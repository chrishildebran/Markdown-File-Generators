﻿namespace Project.ApplicationRequirements.Models;

using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;
using Kelly.Data.TransferServices;
using Project.ApplicationRequirements.DataTransferObjects;

public record ApplicationModel : IDomainModel
{

    public ApplicationModel(ApplicationDataTransferObject application, SqlServerService dataTransferService)
    {
        this.Id               = application.Id;
        this.Name             = application.Name;
        this.NameFormal       = application.NameFormal;
        this.Description      = application.Description;
        this.Owner            = application.Owner;
        this.Language         = application.Language;
        this.LinearUrl        = application.LinearUrl;
        this.RepositoryUrl    = application.RepositoryUrl;
        this.Notes            = application.Notes;
        this.SolutionNameCalc = application.SolutionNameCalc;


        // Get Application Tools

        var parameters = new
        {
            ApplicationId = this.Id
        };

        Console.WriteLine("Retrieving Application Tools");

        var applicationTools = dataTransferService.SelectMany<ApplicationToolDataTransferObject>("[namers].[USP_Tool_SelectWhereApplicationIdEquals_v20250116]", parameters);

        Console.WriteLine("Retrieving Application Tool Requirements");

        foreach (var applicationTool in applicationTools)
        {
            // Create Application Tool Models
            this.ApplicationToolModels.Add(new ApplicationToolModel(applicationTool, dataTransferService));
        }

        Console.WriteLine("Retrieving Application Tool Requirements Tests");
    }

    public List<ApplicationToolModel> ApplicationToolModels{get; set;} = new();

    public string Description{get; set;}

    public int Id{get; set;}

    public string Language{get; set;}

    public string LinearUrl{get; set;}

    public string Name{get; set;}

    public string NameFormal{get; set;}

    public string Notes{get; set;}

    public string Owner{get; set;}

    public string RepositoryUrl{get; set;}

    public string SolutionNameCalc{get; set;}

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        if (o is not ApplicationDataTransferObject application)
        {
            throw new InvalidOperationException();
        }

        this.Id               = application.Id;
        this.Name             = application.Name;
        this.NameFormal       = application.NameFormal;
        this.Description      = application.Description;
        this.Owner            = application.Owner;
        this.Language         = application.Language;
        this.LinearUrl        = application.LinearUrl;
        this.RepositoryUrl    = application.RepositoryUrl;
        this.Notes            = application.Notes;
        this.SolutionNameCalc = application.SolutionNameCalc;
    }

}