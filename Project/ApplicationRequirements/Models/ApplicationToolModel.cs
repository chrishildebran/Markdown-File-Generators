﻿namespace Project.ApplicationRequirements.Models;

using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;
using Kelly.Data.TransferServices;
using Microsoft.IdentityModel.Tokens;
using Project.ApplicationRequirements.DataTransferObjects;

public class ApplicationToolModel : IDomainModel
{

    public ApplicationToolModel(ApplicationToolDataTransferObject applicationToolDataTransferObject, SqlServerService dataTransferService)
    {
        this.ApplicationId          = applicationToolDataTransferObject.ApplicationId;
        this.Noun                   = applicationToolDataTransferObject.Noun;
        this.RootDirectory          = applicationToolDataTransferObject.RootDirectory;
        this.SubNoun                = applicationToolDataTransferObject.NounSecondary;
        this.NounTertiary           = applicationToolDataTransferObject.NounTertiary;
        this.Verb                   = applicationToolDataTransferObject.Verb;
        this.ClassNameActual        = applicationToolDataTransferObject.ClassNameActual;
        this.TriggeredBy            = applicationToolDataTransferObject.TriggeredBy;
        this.Triggers               = applicationToolDataTransferObject.Triggers;
        this.Id                     = applicationToolDataTransferObject.Id;
        this.LinearLabelActual      = applicationToolDataTransferObject.LinearLabelActual;
        this.Name                   = applicationToolDataTransferObject.Name;
        this.Notes                  = applicationToolDataTransferObject.Notes;
        this.ProjectDirectoryActual = applicationToolDataTransferObject.ProjectDirectoryActual;
        this.ScopeDocument          = applicationToolDataTransferObject.ScopeDocument;
        this.ScopeSelection         = applicationToolDataTransferObject.ScopeSelection;
        this.ScopeView              = applicationToolDataTransferObject.ScopeView;
        this.ScopeApplication       = applicationToolDataTransferObject.ScopeApplication;
        this.UpNoteNameActual       = applicationToolDataTransferObject.UpNoteNameActual;


        // Create Application Tool Requirements
        var parameters = new
        {
            ToolId = this.Id
        };

        var pathAndDirectory = dataTransferService.SelectOne<ToolClassAndPathViewDataTransferObject>("[namers].[USP_ToolClassAndPathView_SelectWhereApplicationToolIdEquals_v20250130]", parameters);

        this.ToolClassAndPathView = new ToolClassAndPathViewModel(pathAndDirectory);

        var requirements = dataTransferService.SelectMany<ApplicationToolRequirementDataTransferObject>("[namers].[USP_ToolRequirement_SelectWhereApplicationToolIdEquals_v20250116]", parameters);

        foreach (var requirement in requirements)
        {
            // Create Application Tool Models
            this.ApplicationToolRequirements.Add(new ApplicationToolRequirementModel(requirement, dataTransferService));
        }
    }

    public int ApplicationId{get; set;}

    public List<ApplicationToolIssueModel> ApplicationToolIssues{get; set;} = new();

    public List<ApplicationToolRequirementModel> ApplicationToolRequirements{get; set;} = new();

    public string ClassNameActual{get; set;}

    public int Id{get; set;}

    public string LinearLabelActual{get; set;}

    public string Name{get; set;}

    public string Notes{get; set;}

    public string Noun{get; set;}

    public string NounTertiary{get; set;}

    public string ProjectDirectoryActual{get; set;}

    public string RootDirectory{get; set;}

    public bool ScopeApplication{get; set;}

    public bool ScopeDocument{get; set;}

    public bool ScopeSelection{get; set;}

    public bool ScopeView{get; set;}

    public string SubNoun{get; set;}

    public ToolClassAndPathViewModel ToolClassAndPathView{get; set;}

    public string TriggeredBy{get; set;}

    public string Triggers{get; set;}

    public string UpNoteNameActual{get; set;}

    public string Verb{get; set;}

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public string GetScopeHash()
    {
        var list = new List<string>
        {
            this.ScopeSelection ? "1" : "0",
            this.ScopeView ? "1" : "0",
            this.ScopeDocument ? "1" : "0",
            this.ScopeApplication ? "1" : "0"
        };

        return string.Join("", list);
    }

    public string GetScopes()
    {
        var list = new List<string>
        {
            this.ScopeSelection ? "Selection " : string.Empty,
            this.ScopeView ? "View " : string.Empty,
            this.ScopeDocument ? "Document " : string.Empty,
            this.ScopeApplication ? "Application" : string.Empty
        };

        return string.Join(',', list.Where(s => s.IsNullOrEmpty() == false));
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        throw new NotImplementedException();
    }

}