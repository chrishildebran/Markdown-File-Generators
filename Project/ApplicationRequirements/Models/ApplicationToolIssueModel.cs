﻿namespace Project.ApplicationRequirements.Models;

using System.Text;
using Project.ApplicationRequirements.DataTransferObjects;
using Kelly.Core.Schnittstellen;
using Kelly.Data.Schnittstellen;

public class ApplicationToolIssueModel : IDomainModel
{

    public ApplicationToolIssueModel(ApplicationToolIssueDataTransferObject dataTransferObject)
    {
        this.ApplicationToolId = dataTransferObject.ApplicationToolId;
        this.Cycle             = dataTransferObject.Cycle;
        this.DateImplemented   = dataTransferObject.DateImplemented;
        this.DateInitiated     = dataTransferObject.DateInitiated;
        this.FeatureNamerId    = dataTransferObject.FeatureNamerId;
        this.Id                = dataTransferObject.Id;
        this.InitiatedBy       = dataTransferObject.InitiatedBy;
        this.Notes             = dataTransferObject.Notes;
        this.Role              = dataTransferObject.Role;
        this.SoThat            = dataTransferObject.SoThat;
        this.WantNeed          = dataTransferObject.WantNeed;
    }

    public int ApplicationToolId{get; set;}

    public string Cycle{get; set;}

    public DateTime DateImplemented{get; set;}

    public DateTime DateInitiated{get; set;}

    public int? FeatureNamerId{get; set;}

    public int Id{get; set;}

    public string InitiatedBy{get; set;}

    public string Notes{get; set;}

    public string Role{get; set;}

    public string SoThat{get; set;}

    public string WantNeed{get; set;}

    public IDataTransferObject GetDataTransferObject()
    {
        throw new NotImplementedException();
    }

    public string GetStory()
    {
        var sb = new StringBuilder();

        sb.Append("As a ").Append(this.Role + ", ").Append("I " + this.WantNeed + " ").Append("so that " + this.SoThat);

        return sb.ToString();
    }

    public void ServerDeleteModel()
    {
        throw new NotImplementedException();
    }

    public void ServerInsertModel()
    {
        throw new NotImplementedException();
    }

    public void ServerSelectModel()
    {
        throw new NotImplementedException();
    }

    public void ServerUpdateModel()
    {
        throw new NotImplementedException();
    }

    public void SetModel(IDataTransferObject o)
    {
        throw new NotImplementedException();
    }

}