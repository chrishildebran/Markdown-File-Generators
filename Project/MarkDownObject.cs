﻿namespace Project;

internal class MarkDownObject
{

    public MarkDownObject(string title, string webPage)
    {
        this.Title   = title;
        this.WebPage = webPage;
    }

    public string Title{get; set;}

    public string WebPage{get; set;}

}