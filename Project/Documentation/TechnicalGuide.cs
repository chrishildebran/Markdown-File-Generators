﻿namespace Project.Documentation;

using System.Text;
using System.Text.RegularExpressions;

public static class TechnicalGuide
{

    public static void Create()
    {
        var titlesList = new List<string>
        {
            "Introduction",
            "Chapter 1 ",
            "Chapter 2 ",
            "Conclusion"
        };

        var sourceFiles = ListOfObjects.GetAllCsFiles(@"C:\Repo\kelly\Kelly-Tools-For-Revit-Project");

        foreach (var sourceFile in sourceFiles)
        {
            var fileInfo = new FileInfo(sourceFile);

            if (fileInfo.DirectoryName is null)
            {
                throw new ArgumentNullException(nameof(fileInfo));
            }

            var targetDirectory = new DirectoryInfo(fileInfo.DirectoryName.Replace("c:\\", @"c:\Temp\", StringComparison.CurrentCultureIgnoreCase));

            if (targetDirectory.Exists == false)
            {
                targetDirectory.Create();
            }


            // Technical Guide
            var fileInfoNameReadable = ChangeToTitleCaseString(fileInfo.Name, false, false);

            var technicalGuide = new StringBuilder();

            technicalGuide.AppendLine($"# {fileInfoNameReadable}");

            technicalGuide.AppendLine();

            technicalGuide.AppendLine("## Description");
            technicalGuide.AppendLine();
            technicalGuide.AppendLine("## Requirements (What Must It Do)");
            technicalGuide.AppendLine("* ...");
            technicalGuide.AppendLine();
            technicalGuide.AppendLine("### Data Store");
            technicalGuide.AppendLine("#### Tables");
            technicalGuide.AppendLine("* ...");
            technicalGuide.AppendLine("#### Stored Procedures");
            technicalGuide.AppendLine("* ...");

            File.WriteAllText($"{targetDirectory}\\{fileInfo.Name}.md", technicalGuide.ToString());
        }
    }

    private static string ChangeToTitleCaseString(string text, bool preserveAcronyms, bool preserveNumbers)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return string.Empty;
        }

        if (!preserveNumbers)
        {
            text = StripAllButAlpha(text);
        }

        var newText = new StringBuilder(text.Length * 2);

        newText.Append(text[0]);

        for (var i = 1; i < text.Length; i++)
        {
            var characterCurrent = text[i];

            if (char.IsUpper(characterCurrent) || char.IsNumber(characterCurrent))
            {
                var currChar = text[i - 1];

                var conditionA = currChar != ' ' && !char.IsUpper(currChar) && !char.IsNumber(currChar);

                var conditionB = preserveAcronyms && char.IsUpper(currChar) && i < text.Length - 1 && !char.IsUpper(text[i + 1]);

                if (conditionA || conditionB)
                {
                    newText.Append(' ');
                }
            }

            newText.Append(characterCurrent);
        }

        return newText.ToString();
    }

    private static string StripAllButAlpha(string str)

    {
        var sb = new StringBuilder();

        foreach (var c in str)
        {
            if (c is >= 'A' and <= 'Z' or >= 'a' and <= 'z')
            {
                sb.Append(c);
            }
        }

        var cleanString = Regex.Replace(sb.ToString(), @"\s+", " ");

        return cleanString;
    }

}