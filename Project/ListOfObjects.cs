﻿namespace Project;

internal class ListOfObjects
{

    public static List<string> GetAllCsFiles(string directoryPath)
    {
        var fileList = new List<string>();

        try
        {
            // Get all .cs files in the current directory
            var files = Directory.GetFiles(directoryPath, "*.cs");

            fileList.AddRange(files);


            // Get all subdirectories in the current directory
            var subDirectories = Directory.GetDirectories(directoryPath);

            foreach (var subDirectory in subDirectories)
            {
                // Recursively get .cs files from subdirectories
                fileList.AddRange(GetAllCsFiles(subDirectory));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }

        return fileList;
    }

    public static List<MarkDownObject> GetList()
    {
        return new List<MarkDownObject>
        {
            new("Application", "https://linear.app/jhkelly/project/application-d8dc7460d817"),
            new("Application Service - Cache", "https://linear.app/jhkelly/project/application-service-cache-88a1f1bf24f4"),
            new("Application Service - Event", "https://linear.app/jhkelly/project/application-service-event-96515ffbaab1"),
            new("Application Service - Ribbon", "https://linear.app/jhkelly/project/application-service-ribbon-ce9ac38af67c"),
            new("Application Service - Session", "https://linear.app/jhkelly/project/application-service-session-5ef151cb03f1"),
            new("Application Service - Setting", "https://linear.app/jhkelly/project/application-service-setting-3aeb351d4a35"),
            new("Application Service - Updater", "https://linear.app/jhkelly/project/application-service-updater-fc8741378189"),
            new("Application Service - User", "https://linear.app/jhkelly/project/application-service-user-e5c3dd3a0a61"),
            new("Developer", "https://linear.app/jhkelly/project/developer-6885ffc0bf7d"),
            new("Handler - Application Initialized", "https://linear.app/jhkelly/project/handler-application-initialized-13d17acd40d3"),
            new("Handler - Document Changed", "https://linear.app/jhkelly/project/handler-document-changed-76faf5bd7375"),
            new("Handler - Document Closed", "https://linear.app/jhkelly/project/handler-document-closed-b99f1d52ac13"),
            new("Handler - Document Closing", "https://linear.app/jhkelly/project/handler-document-closing-18b45c08bade"),
            new("Handler - Document Created", "https://linear.app/jhkelly/project/handler-document-created-369c6abb7291"),
            new("Handler - Document Creating", "https://linear.app/jhkelly/project/handler-document-creating-4a5eabd79523"),
            new("Handler - Document Opened", "https://linear.app/jhkelly/project/handler-document-opened-0c75f37721c9"),
            new("Handler - Document Opening", "https://linear.app/jhkelly/project/handler-document-opening-a6f58f9cc77f"),
            new("Handler - Document Reloaded Latest", "https://linear.app/jhkelly/project/handler-document-reloaded-latest-8e9b65d24220"),
            new("Handler - Document Reloading Latest", "https://linear.app/jhkelly/project/handler-document-reloading-latest-92b98d027062"),
            new("Handler - Document Synchronized", "https://linear.app/jhkelly/project/handler-document-synchronized-51ea2b072e13"),
            new("Handler - Document Synchronized", "https://linear.app/jhkelly/project/handler-document-synchronized-72531006737c"),
            new("Handler - Document Synchronizing", "https://linear.app/jhkelly/project/handler-document-synchronizing-a282d3bddb3c"),
            new("Handler - Fabrication Part Browser Changed", "https://linear.app/jhkelly/project/handler-fabrication-part-browser-changed-1af10c7e87be"),
            new("Handler - Failures Processing", "https://linear.app/jhkelly/project/handler-failures-processing-88c585147616"),
            new("Handler - Idling Event", "https://linear.app/jhkelly/project/handler-idling-event-497c2d41dd6b"),
            new("Handler - Linked Resource Opened", "https://linear.app/jhkelly/project/handler-linked-resource-opened-eb8efec4ed40"),
            new("Handler - Linked Resource Opening", "https://linear.app/jhkelly/project/handler-linked-resource-opening-d0fc8b86c37d"),
            new("Handler - View Activated", "https://linear.app/jhkelly/project/handler-view-activated-5e428ce4a7db"),
            new("Handler - View Activating", "https://linear.app/jhkelly/project/handler-view-activating-7a45aedfea22"),
            new("Project Service - Session", "https://linear.app/jhkelly/project/project-service-session-ec427d285110"),
            new("Tool - About", "https://linear.app/jhkelly/project/tool-about-b39224bebd4d"),
            new("Tool - AnyConnect (Vic)", "https://linear.app/jhkelly/project/tool-anyconnect-vic-9c958cbe3dd9"),
            new("Tool - Arrangers", "https://linear.app/jhkelly/project/tool-arrangers-5c7fb23fe2c2"),
            new("Tool - Assembly Creator", "https://linear.app/jhkelly/project/tool-assembly-creator-45dd07b7c2e4"),
            new("Tool - Assembly Manager", "https://linear.app/jhkelly/project/tool-assembly-manager-0e5340e13a91"),
            new("Tool - Assembly Sheets Opener", "https://linear.app/jhkelly/project/tool-assembly-sheets-opener-48a347741aba"),
            new("Tool - Auto Updater", "https://linear.app/jhkelly/project/tool-auto-updater-a572cad2458d"),
            new("Tool - Auto Updater State", "https://linear.app/jhkelly/project/tool-auto-updater-state-61358a3c1b9f"),
            new("Tool - Category Export To Sql Server", "https://linear.app/jhkelly/project/tool-category-export-to-sql-server-49720461b174"),
            new("Tool - Component Data Exporter", "https://linear.app/jhkelly/project/tool-component-data-exporter-78a00c158284"),
            new("Tool - Detailing Package Assigner", "https://linear.app/jhkelly/project/tool-detailing-package-assigner-61af7e16e439"),
            new("Tool - Detailing Package Hider", "https://linear.app/jhkelly/project/tool-detailing-package-hider-5c409f715e59"),
            new("Tool - Detailing Package Isolator", "https://linear.app/jhkelly/project/tool-detailing-package-isolator-55acfec47080"),
            new("Tool - Disconnect (Vic)", "https://linear.app/jhkelly/project/tool-disconnect-vic-3992233d537a"),
            new("Tool - Duct Item Number Creator", "https://linear.app/jhkelly/project/tool-duct-item-number-creator-1749e3e5265c"),
            new("Tool - Duct Item Number Resetter", "https://linear.app/jhkelly/project/tool-duct-item-number-resetter-56313ec570ac"),
            new("Tool - Element Nuker", "https://linear.app/jhkelly/project/tool-element-nuker-06e077b76b8f"),
            new("Tool - External Component Data Exporter", "https://linear.app/jhkelly/project/tool-external-component-data-exporter-4ab99c6159ac"),
            new("Tool - External Component Data Importer", "https://linear.app/jhkelly/project/tool-external-component-data-importer-770cd1a004b0"),
            new("Tool - Fabrication Part Data Exporter", "https://linear.app/jhkelly/project/tool-fabrication-part-data-exporter-dcca989db327"),
            new("Tool - Family Exporter", "https://linear.app/jhkelly/project/tool-family-exporter-f36a9b25e8e5"),
            new("Tool - Family Importer", "https://linear.app/jhkelly/project/tool-family-importer-68ed58f7336a"),
            new("Tool - Family Parameter Editor", "https://linear.app/jhkelly/project/tool-family-parameter-editor-ad80999b81c8"),
            new("Tool - Family Purger", "https://linear.app/jhkelly/project/tool-family-purger-1af0a0cf6653"),
            new("Tool - Field Fit Tack Weld Location Setter", "https://linear.app/jhkelly/project/tool-field-fit-tack-weld-location-setter-9debbb0597a5"),
            new("Tool - Field Fit Weld Weld Location Setter", "https://linear.app/jhkelly/project/tool-field-fit-weld-weld-location-setter-6a0efd2834c0"),
            new("Tool - Field Weld Location Setter", "https://linear.app/jhkelly/project/tool-field-weld-location-setter-22ad3536a092"),
            new("Tool - Hanger Placement", "https://linear.app/jhkelly/project/tool-hanger-placement-23b1ddd76fc8"),
            new("Tool - Ifc Assembly In View Spooler", "https://linear.app/jhkelly/project/tool-ifc-assembly-in-view-spooler-bfd8003c1529"),
            new("Tool - Layout Grid Point Creator", "https://linear.app/jhkelly/project/tool-layout-grid-point-creator-eb32df0f5552"),
            new("Tool - Layout Grid Point Updater", "https://linear.app/jhkelly/project/tool-layout-grid-point-updater-be3e4eb66a78"),
            new("Tool - Layout Point Directory", "https://linear.app/jhkelly/project/tool-layout-point-directory-075ca6082e48"),
            new("Tool - Layout Point Exporter", "https://linear.app/jhkelly/project/tool-layout-point-exporter-3fb4943cfbea"),
            new("Tool - Layout Point Importer", "https://linear.app/jhkelly/project/tool-layout-point-importer-16de0578bb9f"),
            new("Tool - Leveler (Vic)", "https://linear.app/jhkelly/project/tool-leveler-vic-78feb5c88c97"),
            new("Tool - Line Number Assigner", "https://linear.app/jhkelly/project/tool-line-number-assigner-fa4938a973e9"),
            new("Tool - Line Number Hider", "https://linear.app/jhkelly/project/tool-line-number-hider-f740aa193f8d"),
            new("Tool - Line Number Isolator", "https://linear.app/jhkelly/project/tool-line-number-isolator-e191f2b9214d"),
            new("Tool - Location Point Reporter", "https://linear.app/jhkelly/project/tool-location-point-reporter-a3d98c0f8a3d"),
            new("Tool - Manual Updater", "https://linear.app/jhkelly/project/tool-manual-updater-f9790aa47549"),
            new("Tool - Parameter Create", "https://linear.app/jhkelly/project/tool-parameter-create-6692ce5efe7d"),
            new("Tool - Parameter Deleter", "https://linear.app/jhkelly/project/tool-parameter-deleter-05d50fd02330"),
            new("Tool - Parameter Reconcile", "https://linear.app/jhkelly/project/tool-parameter-reconcile-0c51b9f01a78"),
            new("Tool - Parameter Update", "https://linear.app/jhkelly/project/tool-parameter-update-f4cdc58df631"),
            new("Tool - Penetration Tagger", "https://linear.app/jhkelly/project/tool-penetration-tagger-1f956db7d58a"),
            new("Tool - Point To Pipe (Vic)", "https://linear.app/jhkelly/project/tool-point-to-pipe-vic-9251b19020a3"),
            new("Tool - Project Directory", "https://linear.app/jhkelly/project/tool-project-directory-985282a75766"),
            new("Tool - Project Information Editor", "https://linear.app/jhkelly/project/tool-project-information-editor-5ac7c81427eb"),
            new("Tool - Racker (Vic)", "https://linear.app/jhkelly/project/tool-racker-vic-7d3e8472de4f"),
            new("Tool - Rotater (Vic)", "https://linear.app/jhkelly/project/tool-rotater-vic-dd34a7d1eb73"),
            new("Tool - Save Collaboration Project As Local Project", "https://linear.app/jhkelly/project/tool-save-collaboration-project-as-local-project-dd0e796e8c48"),
            new("Tool - Service Assigner", "https://linear.app/jhkelly/project/tool-service-assigner-dd4f417d1bd3"),
            new("Tool - Service Hider", "https://linear.app/jhkelly/project/tool-service-hider-ac46ce5a4568"),
            new("Tool - Service Isolate", "https://linear.app/jhkelly/project/tool-service-isolate-0b741221a69c"),
            new("Tool - Shared Parameter Element Purger", "https://linear.app/jhkelly/project/tool-shared-parameter-element-purger-7e38275a206e"),
            new("Tool - Shop Tack Weld Location Setter", "https://linear.app/jhkelly/project/tool-shop-tack-weld-location-setter-224d299ac1f6"),
            new("Tool - Shop Weld Location Setter", "https://linear.app/jhkelly/project/tool-shop-weld-location-setter-82b4ca39dedd"),
            new("Tool - Strut Assembly In Project Spooler", "https://linear.app/jhkelly/project/tool-strut-assembly-in-project-spooler-640970ae9803"),
            new("Tool - Support Tagger", "https://linear.app/jhkelly/project/tool-support-tagger-dccac87ccbcc"),
            new("Tool - System Assigner", "https://linear.app/jhkelly/project/tool-system-assigner-21e2af335ce2"),
            new("Tool - System Hider", "https://linear.app/jhkelly/project/tool-system-hider-8f26304ac871"),
            new("Tool - System Isolator", "https://linear.app/jhkelly/project/tool-system-isolator-25c68a8f14fb"),
            new("Tool - Updater Reporter", "https://linear.app/jhkelly/project/tool-updater-reporter-0f16d73b4fd7"),
            new("Tool - View Filters Apply By Module Number", "https://linear.app/jhkelly/project/tool-view-filters-apply-by-module-number-222e9a7cdfed"),
            new("Tool - View Filters Apply By Service Name", "https://linear.app/jhkelly/project/tool-view-filters-apply-by-service-name-6f35b32d447f"),
            new("Tool - View Filters Apply By Spool Name", "https://linear.app/jhkelly/project/tool-view-filters-apply-by-spool-name-c639016a86d8"),
            new("Tool - View Filters Apply By System Name", "https://linear.app/jhkelly/project/tool-view-filters-apply-by-system-name-f65d0e54f3cf"),
            new("Tool - View Filters Delete All From Project", "https://linear.app/jhkelly/project/tool-view-filters-delete-all-from-project-1d37c769d20c"),
            new("Tool - View Filters Delete Unused From Project", "https://linear.app/jhkelly/project/tool-view-filters-delete-unused-from-project-10422c81f3a5"),
            new("Tool - View Filters Remove All From View", "https://linear.app/jhkelly/project/tool-view-filters-remove-all-from-view-10fa1abb9ebd"),
            new("Tool - View Opener", "https://linear.app/jhkelly/project/tool-view-opener-a2f6a761d7cb"),
            new("Tool - View Schedule Templates Cleanup", "https://linear.app/jhkelly/project/tool-view-schedule-templates-cleanup-daa56bb745b4"),
            new("Tool - View Schedule Templates Transfer", "https://linear.app/jhkelly/project/tool-view-schedule-templates-transfer-36701f01bdb9"),
            new("Tool - View Schedules To Csv Exporter", "https://linear.app/jhkelly/project/tool-view-schedules-to-csv-exporter-0264784e14ec"),
            new("Tool - View Schedules To Multiple Excel Exporter", "https://linear.app/jhkelly/project/tool-view-schedules-to-multiple-excel-exporter-7608ebeb867d"),
            new("Tool - View Schedules To Single Excel Exporter", "https://linear.app/jhkelly/project/tool-view-schedules-to-single-excel-exporter-1c592f183925"),
            new("Updaters - All", "https://linear.app/jhkelly/project/updaters-all-74707ad78293"),
            new("Updaters - Bill Of Material Type", "https://linear.app/jhkelly/project/updaters-bill-of-material-type-92e8fb2ca1d8"),
            new("Updaters - Checkbox Default", "https://linear.app/jhkelly/project/updaters-checkbox-default-dfb0f1fb7582"),
            new("Updaters - Component", "https://linear.app/jhkelly/project/updaters-component-73387618f0a9"),
            new("Updaters - Content Identity", "https://linear.app/jhkelly/project/updaters-content-identity-540b3de7b175"),
            new("Updaters - Conveyance Electrical", "https://linear.app/jhkelly/project/updaters-conveyance-electrical-05dafab23d48"),
            new("Updaters - Descriptions", "https://linear.app/jhkelly/project/updaters-descriptions-97ee5a76d2ff"),
            new("Updaters - Detailing Package", "https://linear.app/jhkelly/project/updaters-detailing-package-8c30d369b524"),
            new("Updaters - Detailing Phase", "https://linear.app/jhkelly/project/updaters-detailing-phase-48f33c3b0b14"),
            new("Updaters - End Preparation", "https://linear.app/jhkelly/project/updaters-end-preparation-2619b08c4ca7"),
            new("Updaters - Fabrication Part Instances", "https://linear.app/jhkelly/project/updaters-fabrication-part-instances-c0b8057f93c4"),
            new("Updaters - Field Trim", "https://linear.app/jhkelly/project/updaters-field-trim-91e1214245b3"),
            new("Updaters - Gauge", "https://linear.app/jhkelly/project/updaters-gauge-1b396c686b39"),
            new("Updaters - Layout Discipline", "https://linear.app/jhkelly/project/updaters-layout-discipline-ab1e03308988"),
            new("Updaters - Line Number", "https://linear.app/jhkelly/project/updaters-line-number-9012cbdc9269"),
            new("Updaters - Material", "https://linear.app/jhkelly/project/updaters-material-700fa99068ab"),
            new("Updaters - Product", "https://linear.app/jhkelly/project/updaters-product-a8571b91db7c"),
            new("Updaters - Service", "https://linear.app/jhkelly/project/updaters-service-3d030443f78a"),
            new("Updaters - Size", "https://linear.app/jhkelly/project/updaters-size-c6de1ef2677e"),
            new("Updaters - Submittal Not Approved", "https://linear.app/jhkelly/project/updaters-submittal-not-approved-2f9810fc4829"),
            new("Updaters - Support Fabrication", "https://linear.app/jhkelly/project/updaters-support-fabrication-c0522e94894d"),
            new("Updaters - System", "https://linear.app/jhkelly/project/updaters-system-04ecc1113de4"),
            new("Updaters - Weld", "https://linear.app/jhkelly/project/updaters-weld-4651fc9dcc4b")
        };
    }

}